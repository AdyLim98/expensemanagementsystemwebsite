<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('namePlan');
            $table->decimal('budgetMonth',8,2)->index();
            $table->string('typePlan')->nullable();
            // $table->decimal('manualCategoryBudget',8,2)->nullable();
            $table->decimal('livingsBudget',8,2)->nullable();
            $table->decimal('savingsBudget',8,2)->nullable();
            $table->decimal('othersBudget',8,2)->nullable();
            $table->timestamps();

            // $table->unsignedInteger('category_id');
            $table->unsignedInteger('user_id');

            // $table->foreign('user_id')
            // ->references('id')->on('users');

            // $table->foreign('category_id')
            // ->references('id')->on('categories');
        });

        DB::table('budgets')->insert([
            ['namePlan' => 'Plan A ', 'budgetMonth' => 1000, 'typePlan' => "50-30-20", 'user_id' => 1 , "created_at" =>  \Carbon\Carbon::now(), "updated_at" => \Carbon\Carbon::now()],
           
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}
