<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // $table->string('name')->index();
            $table->decimal('amount',8,2)->index();
            $table->date('date')->index();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->unsignedInteger('budget_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('user_id');

            $table->foreign('budget_id')
            ->references('id')->on('budgets');

            $table->foreign('category_id')
            ->references('id')->on('categories');

            // $table->foreign('user_id')
            // ->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
