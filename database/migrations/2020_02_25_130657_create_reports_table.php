<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('expense_id');
            $table->unsignedInteger('income_id');
            $table->unsignedInteger('user_id');

            $table->foreign('expense_id')
            ->references('id')->on('expenses');

            $table->foreign('income_id')
            ->references('id')->on('incomes');

            // $table->foreign('user_id')
            // ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
