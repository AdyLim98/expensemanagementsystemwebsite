<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    //
	protected $fillable = [
		'namePlan',
		'budgetMonth',
		'typePlan',
		// 'manualCategoryBudget',
		'savingsBudget',
		'livingsBudget',
		'othersBudget',
		'user_id',
		// 'category_id',
	];
	

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function expenses()
    {
        return $this->hasMany(Expense::class);
    }
	
    public function notifications()
    {
    	return $this->hasMany(Notification::class);
    }

    // public function category()
    // {
    // 	return $this->belongsTo(Category::class);
    // }
}
