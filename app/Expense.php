<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    //
	protected $fillable = [
		'amount',
		'date',
		'description',
		'budget_id',
		'category_id',
		'user_id',
		// 'name',
		

	];
	

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function category()
	{
		return $this->belongsTo(Category::class);
	}
	
    public function notification()
    {
        return $this->hasOne(Notification::class);
    }

    public function reports()
    {
    	return $this->hasMany(Report::class);
    }

	public function budget()
    {
        return $this->belongsTo(Budget::class);
    }

	
}
