<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //
    protected $fillable = [
		'expense_id',
		'income_id',
        'user_id',
	];
	

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function expense()
    {
    	return $this->belongsTo(Expense::class);
    }

    public function income()
    {
        return $this->belongsTo(Income::class);
    }
}
