<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
	protected $fillable = [
		'amount',
		'date',
		'description',
		'user_id',
	];
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	
    public function notification()
    {
        return $this->hasOne(Notification::class);
    }

    public function reports()
    {
    	return $this->hasMany(Report::class);
    }
}
