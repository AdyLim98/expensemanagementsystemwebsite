<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Budget;
use App\Expense;
use Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class ExpenseApiController extends Controller
{
    //
    public function getCategory()
    {
    	$category = Category::all();
    	
    	if($category)
    	{
    		return response()->json(['data'=>$category],200);
    	}else{
    		return response()->json(['error'=>'No Such Data'], 401);
    	}
    }

    public function getBudget()
    {
    	$budget = Budget::all();
    	
    	if($budget)
    	{
    		return response()->json(['data'=>$budget],200);
    	}else{
    		return response()->json(['error'=>'No Such Data'], 401);
    	}	
    }

	public function index()
	{
		$expenses = Expense::orderBy('date', 'desc')->get();
		
		if($expenses)
		{
			// $expenses->category_id->
			return response()->json(['data'=>$expenses],200); 
		}
		else
		{
			return response()->json(['error'=>'No Such Data'], 401);
		}

		// return view('expense');
	}

    public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [ 
			'budget_id'=>'required',
			'category_id'=>'required',
			'amount' =>'required',
			'date'  =>'required',
			'description' => 'required',

		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}

		$expense = new Expense;
		// $expense->category = $request->input('category');
		$expense->budget_id = $request->input('budget_id');
		$expense->category_id = $request->input('category_id');
		$expense->amount = $request->input('amount');
		$expense->date = Carbon::parse($request->input('date'));
		$expense->description = $request->input('description');
		$expense->user_id = auth()->id();
		
		$result = $expense->save();

		if($result == 1){
			return response()->json(['success'=> $result],200);
		}

	}

	//haveent test yet
	public function destroy($id)
	{
		$expense = Expense::find($id);
		// $expense =Expense::where('id',$id)->first();
		if($expense != null){
		$expense->delete();
		
		return response()->json(['success'=> $expense],200);
		}

	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [ 
			'budget_id'=>'required',
			'category_id'=>'required',
			'amount' =>'required',
			'date'  =>'required',
			'description' => 'required',

		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}
		
		$expense = Expense::find($id);
		// if(!$expense) throw new ModelNotFoundException;
		$expense->budget_id = $request->input('budget_id');
		$expense->category_id = $request->input('category_id');
		$expense->amount = $request->input('amount');
		$expense->date = Carbon::parse($request->input('date'));
		$expense->description = $request->input('description');
		$expense->user_id = auth()->id();

		$result = $expense->save();

		if($result == 1){
			return response()->json(['success'=> $result],200);
		}
	}

	public function show($id)
	{
		$expense = Expense::find($id);
	   	if($expense)
    	{
    		return response()->json(['data'=>$expense],200);
    	}else{
    		return response()->json(['error'=>'No Such Data'], 401);
    	}
	}

	public function sort(Request $request){
		$category = $request->input('category');
		$result = Expense::where('category_id',$category)->get();
		return response()->json(['data'=>$result],200);
	}
}
