<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Budget;
use App\Expense;
use Validator;
use Illuminate\Support\Facades\Input;

class BudgetApiController extends Controller
{
    //
    public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [ 
			'namePlan'=>'required',
			'budgetMonth' =>'required',

		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}
		$budget = new Budget;

		$budget->namePlan = $request->input('namePlan');
		$budget->budgetMonth = $request->input('budgetMonth');
		$budget->typePlan = $request->input('typePlan');
		$budget->livingsBudget = $request->input('livingsBudget');
		$budget->savingsBudget = $request->input('savingsBudget');
		$budget->othersBudget = $request->input('othersBudget');
		$budget->user_id = auth()->id();

		$result = $budget->save();

		if($result == 1){
			return response()->json(['success'=> $result],200);
		}

	}

	public function index(Request $request)
	{
		$userPlan = $request->input('budget');

		return $userPlan;

	}
}
