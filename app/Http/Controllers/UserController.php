<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Image;

class UserController extends Controller
{
    //
	public function profile()
	{
		return view('settingContent',array('user' => Auth::user()) );
	}
	
	public function update_avatar(Request $request)
	{
		//Handle the user upload of avatar
		if($request->hasfile('image')){
			$file = $request->file('image');
			$extension = $file->getClientOriginalExtension();
			$filename = time() . '.' . $extension;
			$file->move('uploads/avatars/',$filename);
			
			$user = Auth::user();
			$user->image = $filename;
		}else{
			// return $request;
			$user->image = '';
		}

		$user->save();

		// if($request->hasFile('avatar')){
		// 	$avatar = $request->file('avatar');
		// 	$filename = time() . '.' . $avatar->getClientOriginalExtension();
		// 	Image::make($avatar)->resize(300,300)->save(public_path('/uploads/avatars/' . $filename));

		// 	$user = Auth::user();
		// 	$user->avatar = $filename;
		// 	$user->save();
		// }
		return view('settingContent',array('user' => Auth::user()) );
	}
}
