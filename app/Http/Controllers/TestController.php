<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Facades\Auth;
class TestController extends Controller
{
    public function testfunction(){

      $name=Input::get('testname');

      if ($name=='laravel') {
        echo "success";

        $notification = array(
                'message' => 'Successfully get laravel data!',
                'alert-type' => 'success'
            );

      } else if ($name=='found') {
        echo "info";

        $notification = array(
                'message' => 'info found data!',
                'alert-type' => 'info'
            );


      } 
      else if ($name=='notfound') {
        echo "warning";
        $notification = array(
                'message' => 'Warning get not found data!',
                'alert-type' => 'warning'
            );

      }else {
        echo "error";
        $notification = array(
                'message' => 'Error! input is empty !',
                'alert-type' => 'error'
            );

      }

      return back()->with($notification);
      
      
    }
    public function index(){
      // return[
      //   "name" =>"Lim Jia Bao",
      //   "email" => "ady98@gmail.com"
      // ];

      // $user = User::find(1);
      // return $user->password;

      return User::all();
    }

    public function insert(Request $request){

      $user = new User();

      $user->name = $request->name;
      $user->email = $request->email;
      $user->password = $request->password;

      if($user->save()){
        return ['status' => 'Data has been inserted'];
      }
    }

    public function login()
    {
      if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
        
        $user = Auth::user();
        $success['token'] =$user->createToken('MyApp')->accessToken;
        return response()->json(['success' => 'Success' ],$this->successStatus);
      
      }else{
        return response()->json(['error'=>'Unauthorized'],401);
      }
    }

    public function details()
    {
      $user = Auth::user();
      return response()->json(['success' => $user]); 
    }

}
