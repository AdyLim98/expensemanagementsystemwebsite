<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use Carbon\Carbon;

class HomeExpenseController extends Controller
{
    //
    public function create()
	{
		
	}
	
	public function store(Request $request)
	{
		$this->validate($request,[
			'budget_id'=>'required',
			'category_id'=>'required',
			'amount' =>'required',
			'date'  =>'required',
			'description' => 'required',
		]);
		$expense = new Expense;
		// $expense->category = $request->input('category');
		$expense->budget_id = $request->input('budget_id');
		$expense->category_id = $request->input('category_id');
		$expense->amount = $request->input('amount');
		$expense->date = Carbon::parse($request->input('date'));
		$expense->description = $request->input('description');
		$expense->user_id = auth()->id();
		
		$expense->save();
		return redirect('home')->with('success','Data Saved');
		// return view('home',[
		// 	'expense' => $expense,
		// ]);
	}

	// public function index()
	// {
	// 	$expenses = Expense::orderBy('date', 'desc')->get();
	// 	$totalExpense = $expenses->sum('amount');
	// 	return view('home', [
	// 		'totalExpense' => $totalExpense
	// 	]);
		
	// }
	
	
}
