<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Illuminate\Support\Facades\Auth;
// use App\helpers;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $notification = array(
                'message' => 'Login Successfully',
                'alert-type' => 'success'
            );
        return back()->with($notification);
    }
    // protected function authenticated() {
    //     $successmessage = 'Hej '.Auth::user()->name.', you are logged in!';
    //     flash()->success('Hello', $successmessage);
    //     return redirect()->intended($this->redirectPath());
    // }
    public function username()
    {
        return 'email';
    }
}
