<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use Carbon\Carbon;
use App\Income;
use App\Http\Controllers\Hash;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::orderBy('date', 'desc')->get();
        // $expenses = $expenses->sum('amount');
        $incomes = Income::all();
        return view('home', [
            'expenses' => $expenses,
            'incomes' => $incomes,
        ]);
    
        // return view('home');
    }

    public function showChangePasswordForm()
    {
        return view('changepassword');
    }
    
    public function changePassword(Request $request){

        if (!(\Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect('setting')->with("success","Password changed successfully !");

    }
    public function changeUsername(Request $request){

        //Change Username
        $user = Auth::user();
        $user->name = ($request->input('new-name'));
        $user->save();

        return redirect('setting')->with("success","Name changed successfully !");

    }

    // public function store(Request $request)
    // {
    //     $this->validate($request,[
    //         // 'category'=>'required',
    //         'amount' =>'required',
    //         'date'  =>'required',
    //         'description' => 'required',
    //     ]);
    //     $expense = new Expense;
    //     // $expense->category = $request->input('category');
    //     $expense->amount = $request->input('amount');
    //     $expense->date = Carbon::parse($request->input('date'));
    //     $expense->description = $request->input('description');

    //     $expense->save();
    //     return redirect('expense')->with('success','Data Saved');
    // }
}
