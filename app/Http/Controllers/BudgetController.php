<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Budget;
use App\Expense;
use \Debugbar;

class BudgetController extends Controller
{
    //
	public function store(Request $request)
	{
		$this->validate($request,[
			'namePlan'=>'required',
			'budgetMonth' =>'required',
		]);
		$budget = new Budget;

		$budget->namePlan = $request->input('namePlan');
		$budget->budgetMonth = $request->input('budgetMonth');
		$budget->typePlan = $request->input('typePlan');
		$budget->livingsBudget = $request->input('livingsBudget');
		$budget->savingsBudget = $request->input('savingsBudget');
		$budget->othersBudget = $request->input('othersBudget');
		$budget->user_id = auth()->id();

		if(empty($budget->typePlan)){
			if($budget->livingsBudget > $budget->budgetMonth){
				session()->flash('notif','Error :Livings Budget Should Not More Than Budget Month');
				return redirect('home');
			}else if($budget->savingsBudget > $budget->budgetMonth){
				session()->flash('notif','Error :Savings Budget Should Not More Than Budget Month');
				return redirect('home');
			}else if($budget->othersBudget > $budget->budgetMonth){
				session()->flash('notif','Error :Others Budget Should Not More Than Budget Month');
				return redirect('home');
			}else if((($budget->livingsBudget+$budget->othersBudget+$budget->savingsBudget))>($budget->budgetMonth)){
				session()->flash('notif','Error : Set Total Budget Should Not More Than Budget Month');
				return redirect('home');
			}else if((($budget->livingsBudget+$budget->othersBudget+$budget->savingsBudget))<($budget->budgetMonth)){
				session()->flash('notif','Error : Set Total Budget Must Equal To Budget Month');
				return redirect('home');
			}
		}
		
		$budget->save();
		// $budgets = Budget::all();
		return redirect('home')->with('success','Budget Plan Saved');
		// return view('home')->with('budgets',$budgets);
		// Return view('index')->with('var1', $var1)
	}

	public function index(Request $request)
	{
		// $budget = null;
		$userPlan = $request->input('budget');
		// if($request->budget){
		// 	$budget = $request->budget;
		// }
		Debugbar::addMessage('hashdoahsdiah',$userPlan);
		// return view('home', ['budget'=>$budget]);
		// $budgets = Budget::all();
		// $expenseBudget = Expense::all(); 
		// Debugbar::addMessage($budget,'budgetinshow');

		// return view("toastLogin",compact('userPlan'));
		// return View::make("toastLogin")->with(array('userPlan'=>$userPlan));

		return view('home', compact('userPlan'));
		// return view('home', [
		// 	'userPlan' => $userPlan,
		// ]);
		// redirect('toastLogin')->with('userPlan',$userPlan);
	}

	public function create()
	{

	}

	public function show($id)
	{
		// $budget = Budget::findOrFail($id);
		// Debugbar::addMessage($id,'123123123');
		// Debugbar::addMessage($budget,'budgetinshow');

		// $budget_id = $budget['id'];
		// $query = Budget::where('id','==',$budget_id)
		// 		->select('budgetMonth');

		// return view('home',[
		// 	'query' => $query;
		// ]);


	}

	public function getBudget(Request $request){
		$budget = new Budget;

		// $userPlan = $request->input('userInputPlan');
		Debugbar::addMessage($userPlan,'SHANE');

		return view('toastLogin',['userPlan'=>$userPlan]);
		// echo "<script type='text/javascript'>alert('BCBBCBCBCBCBCsS');</script>";
		// redirect('home')->with('userPlan',$userPlan);

	}

	public function edit($id)
	{

	}

	public function update(Request $request, $id)
	{

	}

	public function destroy($id)
	{


	}
}
