    <div class="navbar-headers">
      
        <a class="navbar-brands" href="{{ url('/') }}">
            <span style="font-size:20px;font-family:Arial, Helvetica, sans-serif;color:white;" class="SmartTitleHover" id="SmartTitle">Smart Expense</span>
        </a>

    </div>
    
    <div style="float:right;padding-top:15px;margin-right:-10%;">
 {{--          <div>
                  <span class="dropdown dropleft">
                  <i class="fa fa-lightbulb" id="dropdownMenuButton"style="color:white;font-size:20px;padding-right:15px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"  style="background-color:#F5F6F7;width:250%;">
                    <div style="background-color:white"><h3 style="font-size:12px;font-family: inherit;padding:5px 0 5px 5px;">Notifications</h3></div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mix Rice</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM1</span><br><b>Total Expense: </b><span>RM9</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mamak</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM5</span><br><b>Total Expense: </b><span>RM5</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>
                    <div style="border-bottom:1px solid #dddfe2;padding-bottom:0px;"class="alert alert-warning" role="alert">
                      <p style="color:red;padding-bottom:10px;text-align: center;font-size:12px;">You had no take record today<i class="fa fa-frown" style="padding-left:10px;"></i></p>
                    </div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mamak</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM5</span><br><b>Total Expense: </b><span>RM5</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>
                    <div style="padding-top:10px;background-color:white;">
                      <p style="text-align:center;margin-bottom:2px;">
                        <a class="dropdown-item" href="{{url('/notification')}}" style="color:blue;font-size:12px;margin-top:-10px;">See All</a>

                      </p>
                    </div>
                  </span>
                  </div> --}}
        <i class="fa fa-lightbulb" id="lightbulb-hover" style="color:white;font-size:20px;padding-right:15px;" onclick="window.location='{{url("/document")}}'"></i>
        <i class="fa fa-sync" id="sync-hover" onclick="refresh()" style="color:white;font-size:20px;padding-right:15px;"></i>
        @if(empty(Auth::user()->image))
        <img src="/uploads/avatars/default.jpg"  id="user-hover" onclick="window.location='{{ url("/setting") }}'" style="color:white;font-size:20px;height:23px;width:25px;border-radius:50%;position:absolute;top:13px;">
        @else
          <img src="{{ asset('uploads/avatars/'. Auth::user()->image) }}" id="user-hover" onclick="window.location='{{ url("/setting") }}'" style="color:white;font-size:20px;height:23px;width:25px;border-radius:50%;position:absolute;top:13px;">
        @endif
     
        {{-- <img src="/uploads/avatars/{{Auth::user()->image}}" id="user-hover" onclick="window.location='{{ url("/setting") }}'" style="color:white;font-size:20px;height:23px;width:25px;border-radius:50%;position:absolute;top:13px;"> --}}
    </div>
