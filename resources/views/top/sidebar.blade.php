<div id="mySidenav" class="sidenav">
	
	<div style="padding-bottom:60px;background-color:darkgray;padding-top:20px;margin-top:-10px;">
		{{-- <img src="/uploads/avatars/{{Auth::user()->avatar}}" style="height:64px;width:64px;border-top-left-radius:30px;border-top-right-radius:30px;border-bottom-left-radius:30px;border-bottom-right-radius:30px;margin:auto;display:block;"> --}}
      @if(empty(Auth::user()->image))
      <img src="/uploads/avatars/default.jpg"   style="height:64px;width:64px;border-top-left-radius:30px;border-top-right-radius:30px;border-bottom-left-radius:30px;border-bottom-right-radius:30px;margin:auto;display:block;">
      @else
      
      <img src="{{ asset('uploads/avatars/'. Auth::user()->image) }}" style="height:64px;width:64px;border-top-left-radius:30px;border-top-right-radius:30px;border-bottom-left-radius:30px;border-bottom-right-radius:30px;margin:auto;display:block;">
      @endif
                    
		
        
		<span style="text-align:center;position:absolute;width:100%;color:white;padding-top:15px;font-size:15px;">{{ Auth::user()->email }}</span>
	</div> 
	<div style="padding-top:10px;">
    	<a href ="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    	<a href="{{url('/home')}}" class="test"><i class="fa fa-windows" style="padding-right:7px;"></i>Dashboard</a>
 
    	<ul style="list-style-type:none;margin-left:-40px;cursor:pointer;">
    		<li><a class="test caret" style="color:white;"><i class="fa fa-money" style="padding-right:5px;margin-left:-2px;"></i>Expense Management<i class="fa fa-sort-down" style="padding-left:15px;"></i></a>
    			<ul class="nested" style="list-style-type:none;">
    				<li><a href="{{url('/expense')}}"class="test" style="font-size:12px;color:white;"><i class="fa fa-arrow-circle-right" style="padding-right:5px;"></i>Expenses</a></li>
    				<li><a href="{{url('/income')}}"class="test" style="font-size:12px;color:white;"><i class="fa fa-arrow-circle-left" style="padding-right:5px;"></i>Income</a></li>
    				<li><a href="{{url('/report')}}"class="test" style="font-size:12px;color:white;"><i class="fa fa-chart-bar" style="padding-right:5px;"></i>Monthly Report</a></li>
    			</ul>
    		</li>
    	</ul>

    	<a href="{{url('/document')}}" class="test"style="margin-top:-10px;"><i class="fa fa-lightbulb" style="padding-right:12px;"></i>Savings tips</a>
    	<a href="{{url('/setting')}}" class="test"><i class="fa fa-cog" style="padding-right:8px;"></i>Settings</a>
    	<a href="{{ route('logout')}}" onclick="event.preventDefault();
    			document.getElementById('logout-form').submit();" class="test"><i class="fa fa-arrow-left" style="padding-right:8px;"></i>Logout
    	</a>
    	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    		{{ csrf_field() }}
    	</form>


   
	</div>
</div>

<span style="font-size:25px;cursor:pointer;color:white;position:absolute;left:0;padding:10px 0px 15px 25px;width:5%;" onclick="openNav()" id="sidemenu">
        <i class="fa fa-bars" id="mySidenavs"></i>
</span>




