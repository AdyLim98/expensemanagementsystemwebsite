<html lang="{{ app()->getLocale() }}">
<head>
   
    <title>Smart Expense</title>

    @include('top.head')
</head>
<body>
   <?php 
      $expenseBudget = App\Expense::all();
      $budgets = App\Budget::all();
      $incomes = App\Income::all();
      $expenses = App\Expense::all();

      if(empty($yearFilter)){

      $yearFilter = '2020';

      $incomesYear = App\Income::whereYear('date',$yearFilter)->sum('amount');
      $expensesYear = App\Expense::whereYear('date',$yearFilter)->sum('amount');

      }else{

      $incomesYear = App\Income::whereYear('date',$yearFilter)->sum('amount');
      $expensesYear = App\Expense::whereYear('date',$yearFilter)->sum('amount');

      }



      if(empty($userPlan)){
      $userPlan = 1;
      $typePlan = null;

        $try = App\Budget::where('id',$userPlan)->first();
        $try2 = App\Expense::where('budget_id',$userPlan)->get();

        $budgetCategory = App\Budget::where('id',$userPlan)->first();

        $category =App\Expense::where('budget_id',$userPlan)->get();
        
        if($category->where('category_id',1)){
            $categoryExpenseLivings = $category->where('category_id',1)->sum('amount');
        }
        if($category->where('category_id',2)){
            $categoryExpenseOthers = $category->where('category_id',2)->sum('amount');
        }
        if($category->where('category_id',3)){
            $categoryExpenseSavings = $category->where('category_id',3)->sum('amount');
        }



        if($budgetCategory->typePlan == '50-30-20'){
            $budget50Livings = ($budgetCategory->budgetMonth)/2;
            $budget30Others = ($budgetCategory->budgetMonth)*(30/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(20/100);
    
        }else if($budgetCategory->typePlan == '75-15-10'){
            $budget50Livings = ($budgetCategory->budgetMonth)*(75/100);
            $budget30Others = ($budgetCategory->budgetMonth)*(15/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(10/100);

        }else if($budgetCategory->typePlan == '60-10-30'){
            $budget50Livings = ($budgetCategory->budgetMonth)*(60/100);
            $budget30Others = ($budgetCategory->budgetMonth)*(10/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(30/100);
        
        }else{
            $budget50Livings = $budgetCategory->livingsBudget;
            $budget30Others = $budgetCategory->othersBudget;
            $budget20Savings = $budgetCategory->savingsBudget; 
        }

      }else{
        $try = App\Budget::where('id',$userPlan)->first();
        $try2 = App\Expense::where('budget_id',$userPlan)->get();

        $budgetCategory = App\Budget::where('id',$userPlan)->first();
        
        $category =App\Expense::where('budget_id',$userPlan)->get();
        
        if($category->where('category_id',1)){
            $categoryExpenseLivings = $category->where('category_id',1)->sum('amount');
        }
        if($category->where('category_id',2)){
            $categoryExpenseOthers = $category->where('category_id',2)->sum('amount');
        }
        if($category->where('category_id',3)){
            $categoryExpenseSavings = $category->where('category_id',3)->sum('amount');
        }





        if($budgetCategory->typePlan == '50-30-20'){
            $budget50Livings = ($budgetCategory->budgetMonth)/(2);
            $budget30Others = ($budgetCategory->budgetMonth)*(30/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(20/100);


        }else if($budgetCategory->typePlan == '75-15-10'){
            $budget50Livings = ($budgetCategory->budgetMonth)*(75/100);
            $budget30Others = ($budgetCategory->budgetMonth)*(15/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(10/100);


        }else if($budgetCategory->typePlan == '60-10-30'){
            $budget50Livings = ($budgetCategory->budgetMonth)*(60/100);
            $budget30Others = ($budgetCategory->budgetMonth)*(10/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(30/100);
        

        }else{
            $budget50Livings = $budgetCategory->livingsBudget;
            $budget30Others = $budgetCategory->othersBudget;
            $budget20Savings = $budgetCategory->savingsBudget;    
        }

    }
    if(empty($yearFilter)) {

    $yearFilter = '2020';

    $totalExpenseMonth1 = App\Expense::whereMonth('date','01')->whereYear('date',$yearFilter)->get();
    $jan = $totalExpenseMonth1->sum('amount');
    
    $totalExpenseMonth2 = App\Expense::whereMonth('date','02')->whereYear('date',$yearFilter)->get();
    $feb = $totalExpenseMonth2->sum('amount');
    
    $totalExpenseMonth3 = App\Expense::whereMonth('date','03')->whereYear('date',$yearFilter)->get();
    $mar = $totalExpenseMonth3->sum('amount');
    
    $totalExpenseMonth4 = App\Expense::whereMonth('date','04')->whereYear('date',$yearFilter)->get();
    $apr = $totalExpenseMonth4->sum('amount');
    
    $totalExpenseMonth5 = App\Expense::whereMonth('date','05')->whereYear('date',$yearFilter)->get();
    $may = $totalExpenseMonth5->sum('amount');
    
    $totalExpenseMonth6 = App\Expense::whereMonth('date','06')->whereYear('date',$yearFilter)->get();
    $jun = $totalExpenseMonth6->sum('amount');
    
    $totalExpenseMonth7 = App\Expense::whereMonth('date','07')->whereYear('date',$yearFilter)->get();
    $jly = $totalExpenseMonth7->sum('amount');
    
    $totalExpenseMonth8 = App\Expense::whereMonth('date','08')->whereYear('date',$yearFilter)->get();
    $aug = $totalExpenseMonth8->sum('amount');
    
    $totalExpenseMonth9 = App\Expense::whereMonth('date','09')->whereYear('date',$yearFilter)->get();
    $sep = $totalExpenseMonth9->sum('amount');
    
    $totalExpenseMonth10 = App\Expense::whereMonth('date','10')->whereYear('date',$yearFilter)->get();
    $oct = $totalExpenseMonth10->sum('amount');
    
    $totalExpenseMonth11 = App\Expense::whereMonth('date','11')->whereYear('date',$yearFilter)->get();
    $nov = $totalExpenseMonth11->sum('amount');
        
    $totalExpenseMonth12 = App\Expense::whereMonth('date','12')->whereYear('date',$yearFilter)->get();
    $dec = $totalExpenseMonth12->sum('amount');


    $totalYear2019 = App\Expense::whereYear('date','2019')->get(); 
    $totalYear2019 = $totalYear2019->sum('amount');

    $totalYear = App\Expense::whereYear('date','2020')->get();
    $totalYear = $totalYear->sum('amount');
    
    $totalYear2021 = App\Expense::whereYear('date','2021')->get(); 
    $totalYear2021 = $totalYear2021->sum('amount');

    $totalYear2022 = App\Expense::whereYear('date','2022')->get(); 
    $totalYear2022 = $totalYear2022->sum('amount');

    $totalYear2023 = App\Expense::whereYear('date','2023')->get(); 
    $totalYear2023 = $totalYear2023->sum('amount');

    }else{

    $totalExpenseMonth1 = App\Expense::whereMonth('date','01')->whereYear('date',$yearFilter)->get();
    $jan = $totalExpenseMonth1->sum('amount');
    
    $totalExpenseMonth2 = App\Expense::whereMonth('date','02')->whereYear('date',$yearFilter)->get();
    $feb = $totalExpenseMonth2->sum('amount');
    
    $totalExpenseMonth3 = App\Expense::whereMonth('date','03')->whereYear('date',$yearFilter)->get();
    $mar = $totalExpenseMonth3->sum('amount');
    
    $totalExpenseMonth4 = App\Expense::whereMonth('date','04')->whereYear('date',$yearFilter)->get();
    $apr = $totalExpenseMonth4->sum('amount');
    
    $totalExpenseMonth5 = App\Expense::whereMonth('date','05')->whereYear('date',$yearFilter)->get();
    $may = $totalExpenseMonth5->sum('amount');
    
    $totalExpenseMonth6 = App\Expense::whereMonth('date','06')->whereYear('date',$yearFilter)->get();
    $jun = $totalExpenseMonth6->sum('amount');
    
    $totalExpenseMonth7 = App\Expense::whereMonth('date','07')->whereYear('date',$yearFilter)->get();
    $jly = $totalExpenseMonth7->sum('amount');
    
    $totalExpenseMonth8 = App\Expense::whereMonth('date','08')->whereYear('date',$yearFilter)->get();
    $aug = $totalExpenseMonth8->sum('amount');
    
    $totalExpenseMonth9 = App\Expense::whereMonth('date','09')->whereYear('date',$yearFilter)->get();
    $sep = $totalExpenseMonth9->sum('amount');
    
    $totalExpenseMonth10 = App\Expense::whereMonth('date','10')->whereYear('date',$yearFilter)->get();
    $oct = $totalExpenseMonth10->sum('amount');
    
    $totalExpenseMonth11 = App\Expense::whereMonth('date','11')->whereYear('date',$yearFilter)->get();
    $nov = $totalExpenseMonth11->sum('amount');
        
    $totalExpenseMonth12 = App\Expense::whereMonth('date','12')->whereYear('date',$yearFilter)->get();
    $dec = $totalExpenseMonth12->sum('amount');
    }


    $totalYear2019 = App\Expense::whereYear('date','2019')->get(); 
    $totalYear2019 = $totalYear2019->sum('amount');

    $totalYear = App\Expense::whereYear('date','2020')->get();
    $totalYear = $totalYear->sum('amount');
    
    $totalYear2021 = App\Expense::whereYear('date','2021')->get(); 
    $totalYear2021 = $totalYear2021->sum('amount');

    $totalYear2022 = App\Expense::whereYear('date','2022')->get(); 
    $totalYear2022 = $totalYear2022->sum('amount');

    $totalYear2023 = App\Expense::whereYear('date','2023')->get(); 
    $totalYear2023 = $totalYear2023->sum('amount');

    ?>

    <div id="app">

        <nav class="navbars navbar-defaults navbar-static-tops" style="background-color:#00A0D2;position:fixed;width:100%;">
            <div class="container">
                @include('top.topbar')
                @include('top.sidebar')
                

            </div>
        </nav>
    
      

        @yield('content')
    </div>

    <!-- Scripts -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
   
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    




    <script type="text/javascript">
        $(document).ready(function(){
           $('#datepicker').datepicker(); 
       });

    </script>
    <script type="text/javascript">
          $("#yearpicker").datepicker( {
            format: " yyyy", // Notice the Extra space at the beginning
            viewMode: "years", 
            minViewMode: "years"
          });
    </script>

    <script>
      

        //for side menu bar
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
          document.getElementById("SmartTitle").style.marginLeft = "60px";
          document.getElementById("monthlyReport").style.marginLeft = "250px";
          document.getElementById("reportDashBoard").style.marginLeft = "60px";

        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
          document.getElementById("SmartTitle").style.marginLeft = "0";
          document.getElementById("monthlyReport").style.marginLeft = "200px";
          document.getElementById("reportDashBoard").style.marginLeft = "0";
        }
        
        function refresh(){
          var x = document.getElementById("snackbar");
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
          location.reload();
        }

        //tree view
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
          toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
      }
    </script>
     <script>
      var ctx = document.getElementById("myChart").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["January", "February", "March", "April", "May", "June","July","August","September","October","November","December"],
          datasets: [{
            label: 'Expenses Monthly',
            data: [{{$jan}},{{$feb}},{{$mar}},{{$apr}},{{$may}},{{$jun}},{{$jly}},{{$aug}},{{$sep}},{{$oct}},{{$nov}},{{$dec}}],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(0,255,255,0.2)',
            'rgba(128,0,0,0.2)',
            'rgba(128,0,128,0.2)',
            'rgba(255,0,255,0.2)',
            'rgba(0,0,128,0.2)',
            'rgba(192,192,192,0.2)',
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(0,255,255,1)',
            'rgba(128,0,0,1)',
            'rgba(128,0,128,1)',
            'rgba(255,0,255,1)',
            'rgba(0,0,128,1)',
            'rgba(192,192,192,1)',

            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    </script>
    <script>
      //line chart
      //line
      var ctxL = document.getElementById("lineChart").getContext('2d');
      var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
          labels: ["2019", "2020", "2021", "2022","2023"],
          datasets: [{
            label: "Total Expenses",
            data: [{{$totalYear2019}},{{$totalYear}},{{$totalYear2021}} ,{{$totalYear2022}},{{$totalYear2023}}],
            backgroundColor: [
            'rgba(105, 0, 132, .2)',
            ],
            borderColor: [
            'rgba(200, 99, 132, .7)',
            ],
            borderWidth: 2
          },

          ]
        },
        options: {
          responsive: true
        }
      });


    </script>
</body>
</html>
