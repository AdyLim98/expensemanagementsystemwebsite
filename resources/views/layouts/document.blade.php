<html>
<head>
    <title>Smart Expense</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/homesidebar.css') }}" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body style="background-color:#EEEEEE;">
    <div id="app">

        <nav class="navbars navbar-defaults navbar-static-tops" style="background-color:#00A0D2;position:fixed;width:100%;">
            <div class="container">
               <div class="navbar-headers">

                    <a class="navbar-brands" style="cursor:pointer;" href="{{ url('/') }}">
                        <span style="font-size:20px;font-family:Arial, Helvetica, sans-serif;color:white;margin-left:-100%;" class="SmartTitleHover" id="SmartTitle">Saving tips</span>
                    </a>

                </div>

                <div style="float:right;padding-top:15px;margin-right:-10%;">
                      {{-- <div>
                  <span class="dropdown dropleft">
                  <i class="fa fa-bell" id="dropdownMenuButton"style="color:white;font-size:20px;padding-right:15px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"  style="background-color:#F5F6F7;width:250%;">
                    <div style="background-color:white"><h3 style="font-size:12px;font-family: inherit;padding:5px 0 5px 5px;">Notifications</h3></div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mix Rice</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM1</span><br><b>Total Expense: </b><span>RM9</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mamak</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM5</span><br><b>Total Expense: </b><span>RM5</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>
                    <div style="border-bottom:1px solid #dddfe2;padding-bottom:0px;"class="alert alert-warning" role="alert">
                      <p style="color:red;padding-bottom:10px;text-align: center;font-size:12px;">You had no take record today<i class="fa fa-frown" style="padding-left:10px;"></i></p>
                    </div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mamak</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM5</span><br><b>Total Expense: </b><span>RM5</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>
                    <div style="padding-top:10px;background-color:white;">
                      <p style="text-align:center;margin-bottom:2px;">
                        <a class="dropdown-item" href="{{url('/notification')}}" style="color:blue;font-size:12px;margin-top:-10px;">See All</a>

                      </p>
                    </div>
                  </span>
                  </div> --}}
                    <i class="fa fa-lightbulb" id="lightbulb-hover" style="color:white;font-size:20px;padding-right:15px;" onclick="window.location='{{url("/document")}}'"></i>
                    <i class="fa fa-sync" onclick="refresh()" id="sync-hover" style="color:white;font-size:20px;padding-right:15px;"></i>

                    @if(empty(Auth::user()->image))
                    <img src="/uploads/avatars/default.jpg"  id="user-hover" onclick="window.location='{{ url("/setting") }}'" style="color:white;font-size:20px;height:23px;width:25px;border-radius:50%;position:absolute;top:13px;">
                    @else
                    <img src="{{ asset('uploads/avatars/'. Auth::user()->image) }}" id="user-hover" onclick="window.location='{{ url("/setting") }}'" style="color:white;font-size:20px;height:23px;width:25px;border-radius:50%;position:absolute;top:13px;">
                    @endif
                    
                    {{-- <img src="/uploads/avatars/{{Auth::user()->avatar}}" id="user-hover" onclick="window.location='{{ url("/setting") }}'" style="color:white;font-size:20px;height:23px;width:25px;border-radius:50%;position:absolute;top:13px;"> --}}
                </div>

                {{-- @include('top.sidebar') --}}
                <div id="mySidenav" class="sidenav">
  
                  <div style="padding-bottom:60px;background-color:darkgray;padding-top:20px;margin-top:-10px;">
                      {{-- <img src="/uploads/avatars/{{Auth::user()->avatar}}" style="height:64px;width:64px;border-top-left-radius:30px;border-top-right-radius:30px;border-bottom-left-radius:30px;border-bottom-right-radius:30px;margin:auto;display:block;"> --}}
                      @if(empty(Auth::user()->image))
                      <img src="/uploads/avatars/default.jpg"   style="height:64px;width:64px;border-top-left-radius:30px;border-top-right-radius:30px;border-bottom-left-radius:30px;border-bottom-right-radius:30px;margin:auto;display:block;">
                      @else
                      
                      <img src="{{ asset('uploads/avatars/'. Auth::user()->image) }}" style="height:64px;width:64px;border-top-left-radius:30px;border-top-right-radius:30px;border-bottom-left-radius:30px;border-bottom-right-radius:30px;margin:auto;display:block;">
                      @endif
                      
                      
                          <span style="text-align:center;position:absolute;width:100%;color:white;padding-top:15px;font-size:15px;">{{ Auth::user()->email }}</span>
                  </div> 

                  <div style="padding-top:10px;">
                      <a href ="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                      <a href="{{url('/home')}}" class="test"><i class="fa fa-windows" style="padding-right:7px;"></i>Dashboard</a>

                      <ul style="list-style-type:none;margin-left:-40px;cursor:pointer;">
                          <li><a class="test caret" style="color:white;"><i class="fa fa-money" style="padding-right:5px;margin-left:-2px;"></i>Expense Management<i class="fa fa-sort-down" style="padding-left:15px;"></i></a>
                            <ul class="nested" style="list-style-type:none;">
                          {{-- <li><a class="test" style="font-size:12px;color:white;"><i class="fa fa-delicious" style="padding-right:5px;"></i>Expense Categories</a></li> --}}
                          {{-- <li><a class="test" style="font-size:12px;color:white;"><i class="fa fa-delicious" style="padding-right:5px;"></i>Income Categories</a></li> --}}
                              <li><a href="{{url('/expense')}}"class="test" style="font-size:12px;color:white;"><i class="fa fa-arrow-circle-right" style="padding-right:5px;"></i>Expenses</a>
                              </li>
                              <li><a href="{{url('/income')}}"class="test" style="font-size:12px;color:white;"><i class="fa fa-arrow-circle-left" style="padding-right:5px;"></i>Income</a></li>
                              <li><a href="{{url('/report')}}"class="test" style="font-size:12px;color:white;"><i class="fa fa-chart-bar" style="padding-right:5px;"></i>Monthly Report</a></li>
                            </ul>
                         </li>
                      </ul>

                      <a href="{{url('/document')}}" class="test"style="margin-top:-10px;"><i class="fa fa-lightbulb" style="padding-right:12px;"></i>Savings tips</a>
                      <a href="{{url('/setting')}}" class="test"><i class="fa fa-cog" style="padding-right:8px;"></i>Settings</a>
                      <a href="{{ route('logout')}}" onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();" class="test"><i class="fa fa-arrow-left" style="padding-right:8px;"></i>Logout
                      </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>

                </div>
              </div>

              <span style="font-size:25px;cursor:pointer;color:white;position:absolute;left:0;padding:10px 0px 15px 25px;width:5%;" onclick="openNav()" id="sidemenu">
                <i class="fa fa-bars" id="mySidenavs"></i>
              </span>

            </div>
        </nav>
  
        @yield('contents')
      
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script src = "https://code.jquery.com/jquery-3.3.1.slim.min.js" 
         integrity = "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
         crossorigin = "anonymous">
      </script>
      
      <script src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" 
         integrity = "sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" 
         crossorigin = "anonymous">
      </script>
      
      <script src = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" 
         integrity = "sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" 
         crossorigin = "anonymous">
      </script>
    <script>

        //for side menu bar
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
          document.getElementById("SmartTitle").style.marginLeft = "50px";
          document.getElementById("carouselExampleControls").style.marginLeft = "100px";
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
          document.getElementById("SmartTitle").style.marginLeft = "-100px";
          document.getElementById("carouselExampleControls").style.marginLeft = "0";
        }

        function refresh(){
          var x = document.getElementById("snackbar");
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
          location.reload();
        }

        //tree view
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
          toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
      }
    </script>
</body>
</html>
