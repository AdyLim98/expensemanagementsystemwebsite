<html lang="{{ app()->getLocale() }}">
<head>
   
    <title>Smart Expense</title>

    @include('top.head')
</head>
<body>
    <div id="app">

        <nav class="navbars navbar-defaults navbar-static-tops" style="background-color:#00A0D2;position:fixed;width:100%;">
            <div class="container">
                @include('top.topbar')
                @include('top.sidebar')
                

            </div>
        </nav>
    
      

        @yield('content')
    </div>

    <!-- Scripts -->
    {{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
   
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
    {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> --}}
    {{-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> --}}
    <script type="text/javascript">
        $(document).ready(function(){
           $('#datepicker').datepicker(); 
       });

    </script>
    <script type="text/javascript">
          $("#yearpicker").datepicker( {
            format: " yyyy", // Notice the Extra space at the beginning
            viewMode: "years", 
            minViewMode: "years"
          });
    </script>
    <script>
      

        //for side menu bar
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
          document.getElementById("SmartTitle").style.marginLeft = "60px";
          document.getElementById("expenseDashBoard").style.marginLeft = "60px";
          document.getElementById("expenseList").style.marginLeft = "60px";
          document.getElementById("successNotificationExpense").style.marginLeft = "60px";
          document.getElementById("failNotificationExpense").style.marginLeft = "60px";
         
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
          document.getElementById("SmartTitle").style.marginLeft = "0";
          document.getElementById("expenseDashBoard").style.marginLeft = "0";
          document.getElementById("expenseList").style.marginLeft = "0";
          document.getElementById("successNotificationExpense").style.marginLeft = "0";
          document.getElementById("failNotificationExpense").style.marginLeft = "60px";
        }
        
        function refresh(){
          var x = document.getElementById("snackbar");
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
          location.reload();
        }

        //tree view
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
          toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
      }
    </script>
    <script type="text/javascript">
      //edit Record
      $(document).ready(function(){
        var table = $('#datatable').DataTable();

        //Start Edit Record
        table.on('click','.edit',function(){
          $tr = $(this).closest('tr');
          if($($tr).hasClass('child')){
            $tr = $tr.prev('.parent');
          }

          var data = table.row($tr).data();
          console.log(data);
          console.log(data[1])
          $('#budget').val(data[0]);
          $('#category').val(data[0]);
          $('#amount').val(data[2]);
          $('#date').val(data[3]);
          $('#description').val(data[4]);

          $('#editForm').attr('action','/expense/'+($('#hiddenId').val()));
        
          $('#editexpenseModal').modal('show');
        });

        //delete record
        table.on('click','.delete',function(){
          $tr = $(this).closest('tr');
          if($($tr).hasClass('child')){
            $tr = $tr.prev('.parent');
          }

          var data = table.row($tr).data();
          console.log(data);

          // $('#id').val(data[0]);

          $('#deleteForm').attr('action','/expense/'+($('#hiddenId').val()));
          $('#deleteexpenseModal').modal('show');

        });
      });

    </script>

</body>
</html>
