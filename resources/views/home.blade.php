<html lang="{{ app()->getLocale() }}">
<head>
   
    <title>Smart Expense</title>
    {{-- try --}}
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    @include('top.head')
</head>
<body>

    <?php 
      $expenseBudget = App\Expense::all();
      $budgets = App\Budget::all();
      $incomes = App\Income::all();
      $expenses = App\Expense::all();

      if(empty($userPlan)){
      $userPlan = 1;
      $typePlan = null;

        $try = App\Budget::where('id',$userPlan)->first();
        $try2 = App\Expense::where('budget_id',$userPlan)->get();

        $budgetCategory = App\Budget::where('id',$userPlan)->first();

        $category =App\Expense::where('budget_id',$userPlan)->get();
        
        if($category->where('category_id',1)){
            $categoryExpenseLivings = $category->where('category_id',1)->sum('amount');
        }
        if($category->where('category_id',2)){
            $categoryExpenseOthers = $category->where('category_id',2)->sum('amount');
        }
        if($category->where('category_id',3)){
            $categoryExpenseSavings = $category->where('category_id',3)->sum('amount');
        }



        if($budgetCategory->typePlan == '50-30-20'){
            $budget50Livings = ($budgetCategory->budgetMonth)/2;
            $budget30Others = ($budgetCategory->budgetMonth)*(30/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(20/100);
    
        }else if($budgetCategory->typePlan == '75-15-10'){
            $budget50Livings = ($budgetCategory->budgetMonth)*(75/100);
            $budget30Others = ($budgetCategory->budgetMonth)*(15/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(10/100);

        }else if($budgetCategory->typePlan == '60-10-30'){
            $budget50Livings = ($budgetCategory->budgetMonth)*(60/100);
            $budget30Others = ($budgetCategory->budgetMonth)*(10/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(30/100);
        
        }else{
            $budget50Livings = $budgetCategory->livingsBudget;
            $budget30Others = $budgetCategory->othersBudget;
            $budget20Savings = $budgetCategory->savingsBudget; 
        }

      }else{
        $try = App\Budget::where('id',$userPlan)->first();
        $try2 = App\Expense::where('budget_id',$userPlan)->get();

        $budgetCategory = App\Budget::where('id',$userPlan)->first();
        
        $category =App\Expense::where('budget_id',$userPlan)->get();
        
        if($category->where('category_id',1)){
            $categoryExpenseLivings = $category->where('category_id',1)->sum('amount');
        }
        if($category->where('category_id',2)){
            $categoryExpenseOthers = $category->where('category_id',2)->sum('amount');
        }
        if($category->where('category_id',3)){
            $categoryExpenseSavings = $category->where('category_id',3)->sum('amount');
        }





        if($budgetCategory->typePlan == '50-30-20'){
            $budget50Livings = ($budgetCategory->budgetMonth)/(2);
            $budget30Others = ($budgetCategory->budgetMonth)*(30/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(20/100);


        }else if($budgetCategory->typePlan == '75-15-10'){
            $budget50Livings = ($budgetCategory->budgetMonth)*(75/100);
            $budget30Others = ($budgetCategory->budgetMonth)*(15/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(10/100);


        }else if($budgetCategory->typePlan == '60-10-30'){
            $budget50Livings = ($budgetCategory->budgetMonth)*(60/100);
            $budget30Others = ($budgetCategory->budgetMonth)*(10/100);
            $budget20Savings = ($budgetCategory->budgetMonth)*(30/100);
        

        }else{
            $budget50Livings = $budgetCategory->livingsBudget;
            $budget30Others = $budgetCategory->othersBudget;
            $budget20Savings = $budgetCategory->savingsBudget;    
        }

    }
    if(empty($yearFilter)) {

    $yearFilter = '2020';

    $totalExpenseMonth1 = App\Expense::whereMonth('date','01')->whereYear('date',$yearFilter)->get();
    $jan = $totalExpenseMonth1->sum('amount');
    
    $totalExpenseMonth2 = App\Expense::whereMonth('date','02')->whereYear('date',$yearFilter)->get();
    $feb = $totalExpenseMonth2->sum('amount');
    
    $totalExpenseMonth3 = App\Expense::whereMonth('date','03')->whereYear('date',$yearFilter)->get();
    $mar = $totalExpenseMonth3->sum('amount');
    
    $totalExpenseMonth4 = App\Expense::whereMonth('date','04')->whereYear('date',$yearFilter)->get();
    $apr = $totalExpenseMonth4->sum('amount');
    
    $totalExpenseMonth5 = App\Expense::whereMonth('date','05')->whereYear('date',$yearFilter)->get();
    $may = $totalExpenseMonth5->sum('amount');
    
    $totalExpenseMonth6 = App\Expense::whereMonth('date','06')->whereYear('date',$yearFilter)->get();
    $jun = $totalExpenseMonth6->sum('amount');
    
    $totalExpenseMonth7 = App\Expense::whereMonth('date','07')->whereYear('date',$yearFilter)->get();
    $jly = $totalExpenseMonth7->sum('amount');
    
    $totalExpenseMonth8 = App\Expense::whereMonth('date','08')->whereYear('date',$yearFilter)->get();
    $aug = $totalExpenseMonth8->sum('amount');
    
    $totalExpenseMonth9 = App\Expense::whereMonth('date','09')->whereYear('date',$yearFilter)->get();
    $sep = $totalExpenseMonth9->sum('amount');
    
    $totalExpenseMonth10 = App\Expense::whereMonth('date','10')->whereYear('date',$yearFilter)->get();
    $oct = $totalExpenseMonth10->sum('amount');
    
    $totalExpenseMonth11 = App\Expense::whereMonth('date','11')->whereYear('date',$yearFilter)->get();
    $nov = $totalExpenseMonth11->sum('amount');
        
    $totalExpenseMonth12 = App\Expense::whereMonth('date','12')->whereYear('date',$yearFilter)->get();
    $dec = $totalExpenseMonth12->sum('amount');


    $totalYear2019 = App\Expense::whereYear('date','2019')->get(); 
    $totalYear2019 = $totalYear2019->sum('amount');

    $totalYear = App\Expense::whereYear('date','2020')->get();
    $totalYear = $totalYear->sum('amount');
    
    $totalYear2021 = App\Expense::whereYear('date','2021')->get(); 
    $totalYear2021 = $totalYear2021->sum('amount');

    $totalYear2022 = App\Expense::whereYear('date','2022')->get(); 
    $totalYear2022 = $totalYear2022->sum('amount');

    $totalYear2023 = App\Expense::whereYear('date','2023')->get(); 
    $totalYear2023 = $totalYear2023->sum('amount');

    }else{

    $totalExpenseMonth1 = App\Expense::whereMonth('date','01')->whereYear('date',$yearFilter)->get();
    $jan = $totalExpenseMonth1->sum('amount');
    
    $totalExpenseMonth2 = App\Expense::whereMonth('date','02')->whereYear('date',$yearFilter)->get();
    $feb = $totalExpenseMonth2->sum('amount');
    
    $totalExpenseMonth3 = App\Expense::whereMonth('date','03')->whereYear('date',$yearFilter)->get();
    $mar = $totalExpenseMonth3->sum('amount');
    
    $totalExpenseMonth4 = App\Expense::whereMonth('date','04')->whereYear('date',$yearFilter)->get();
    $apr = $totalExpenseMonth4->sum('amount');
    
    $totalExpenseMonth5 = App\Expense::whereMonth('date','05')->whereYear('date',$yearFilter)->get();
    $may = $totalExpenseMonth5->sum('amount');
    
    $totalExpenseMonth6 = App\Expense::whereMonth('date','06')->whereYear('date',$yearFilter)->get();
    $jun = $totalExpenseMonth6->sum('amount');
    
    $totalExpenseMonth7 = App\Expense::whereMonth('date','07')->whereYear('date',$yearFilter)->get();
    $jly = $totalExpenseMonth7->sum('amount');
    
    $totalExpenseMonth8 = App\Expense::whereMonth('date','08')->whereYear('date',$yearFilter)->get();
    $aug = $totalExpenseMonth8->sum('amount');
    
    $totalExpenseMonth9 = App\Expense::whereMonth('date','09')->whereYear('date',$yearFilter)->get();
    $sep = $totalExpenseMonth9->sum('amount');
    
    $totalExpenseMonth10 = App\Expense::whereMonth('date','10')->whereYear('date',$yearFilter)->get();
    $oct = $totalExpenseMonth10->sum('amount');
    
    $totalExpenseMonth11 = App\Expense::whereMonth('date','11')->whereYear('date',$yearFilter)->get();
    $nov = $totalExpenseMonth11->sum('amount');
        
    $totalExpenseMonth12 = App\Expense::whereMonth('date','12')->whereYear('date',$yearFilter)->get();
    $dec = $totalExpenseMonth12->sum('amount');
    }


    $totalYear2019 = App\Expense::whereYear('date','2019')->get(); 
    $totalYear2019 = $totalYear2019->sum('amount');

    $totalYear = App\Expense::whereYear('date','2020')->get();
    $totalYear = $totalYear->sum('amount');
    
    $totalYear2021 = App\Expense::whereYear('date','2021')->get(); 
    $totalYear2021 = $totalYear2021->sum('amount');

    $totalYear2022 = App\Expense::whereYear('date','2022')->get(); 
    $totalYear2022 = $totalYear2022->sum('amount');

    $totalYear2023 = App\Expense::whereYear('date','2023')->get(); 
    $totalYear2023 = $totalYear2023->sum('amount');

    ?>

    <div id="app">

        <nav class="navbars navbar-defaults navbar-static-tops" style="background-color:#00A0D2;position:fixed;width:100%;">
            <div class="container">
                @include('top.topbar')
                @include('top.sidebar')
                

            </div>
        </nav>

        <div style="font-size:15px;font-family:Arial, Helvetica, sans-serif;border-bottom:1px solid #DDDDDD;padding:14px 15px;margin-top:-20px;color:#818181;padding-left:14%;padding-top:6%;position:absolute;width:100%;" id="dashboard-home">DashBoard
            <div style="float:right;padding-right:40px;">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#incomeModal"><i class="fa fa-plus"style="padding-right:5px;"></i>Category</button>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#expenseModal"><i class="fa fa-plus" style="padding-right:5px;"></i>Expense</button>
            </div>
        </div>

        <div id="snackbar">Syncing .....</div>
        <div class="modal fade" id="incomeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                        <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Add New Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>


                  <form action="{{action('CategoryController@store')}}" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" style="font-size:14px;padding-top:12px;font-weight:600;">Category</label>
                            <div class="col-sm-10" style="width:70%;padding-top:9px;">
                                <input type="text" name="category" class="form-control" style="width:60%;font-size:12px;height:30px;">
                            </div>
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;margin-bottom:-7px;">Close</button>
                        <button type="submit" class="btn btn-primary" style="font-size:12px;margin-bottom:-7px;">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php 
    $budgets = App\Budget::all();
    $categories = App\Category::all();
    ?>

    {{-- Expense Modal popup --}}
    <div class="modal fade" id="expenseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                    <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Add New Expenses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <form action="{{action('HomeExpenseController@store')}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group row" style="padding-top:10px;padding-bottom:20px;" id="plan-box">
                        <label class="col-sm-3 col-form-label" style="font-size:13px;padding-top:20px;font-weight:600;">Plan Name <span style="font-size:11px;"> (Created)</span>
                        </label>

                        <div class="col-sm-4" style="padding-top:22px;">
                            <select id="inputState" name="budget_id" class="form-control fa" style="font-size:15px;padding-top:2px;width:auto;">
                                @foreach($budgets as $budget)

                                <option value="{{$budget->id}}">{{$budget->namePlan}}</option>

                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-3 col-form-label" style="font-size:13px;padding-top:7px;font-weight:600;">Categories</label>
                        <div class="form-group col-md-4" style="padding-top:5px;">
                            <select id="inputState" name="category_id"class="form-control fa" style="font-size:15px;padding-top:2px;width:auto;">
                                @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->type}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label class="col-sm-3 col-form-label" style="font-size:13px;padding-top:7px;font-weight:600;">Amount (RM)</label>
                        <div class="col-sm-4" style="width:70%;padding-top:4px;">
                            <input type="number" name="amount" class="form-control" style="width:100%;font-size:12px;height:30px;">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" style="font-size:13px;padding-top:25px;font-weight:600;">Date</label>
                        <div class="col-sm-4" style="padding-top:22px;">
                            <input type='text' class="form-control" data-provide="datepicker" style="width:100%;font-size:12px;background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;cursor:pointer;" name="date">
                        </div>
                    </div>

                    <div class="form-group row" style="padding-top:10px;">
                        <label for="exampleFormControlTextarea1" class="col-sm-3 col-form-label" style="font-size:13px;padding-top:25px;font-weight:600;">Description</label>
                        <div class="col-sm-4" style="padding-top:25px;">
                            <textarea class="form-control" style="font-size:12px;width:100%;" id="exampleFormControlTextarea1" rows="3" name="description"></textarea>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;">Close</button>
                    <button type="submit" class="btn btn-primary" style="font-size:12px;margin-top:-1px;">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div style="padding-top:7%;">
    @if(count($errors)>0)

    <div class="alert alert-danger" id="failNotificationExpense">
      <ul style="font-size:15px;font-family:Arial, Helvetica, sans-serif;padding-left:13.5%;">
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

@if(\Session::has('success'))
<div class="alert alert-success" id="successNotificationExpense">
  <p style="font-size:15px;font-family:Arial, Helvetica, sans-serif;padding-left:13.5%;">{{ \Session::get('success')}}</p>
</div>
@endif

<div class="container" style="padding-top:4px;margin-right:11.5%;">
    @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-danger" style="width:97%;font-size:13px;">
            {{session()->get('notif')}}
        </div>
    </div>
    @endif
</div>
</div>
{{-- budget design Pie --}}
<div class="container" id="recommendationPlan" style="padding-top:1%;">

    <div>
        <div>
            <p style="font-size:15px;font-family:Arial, Helvetica, sans-serif;"><i class="fa fa-star" style="padding-right:10px;"></i>Recommendation Plan</p>
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size:15px;background-color:rgba(0,0,0,.03);font-family:Arial, Helvetica, sans-serif">Budgeting
                   <button type="button" class="btn btn-success" style="font-size:12px;float:right;margin-top:-2px;" data-toggle="modal" data-target="#planModal">Plan</button>
               </div>

               <div class="panel-body">
           {{--      <form method="GET" id="budgetForm" action="{{action('BudgetController@index')}}">  
                    {{csrf_field()}}

                    <div style="float:right;">
                        <select name="budget" id="budget" class="form-control" onchange="document.getElementById('budgetForm').submit()">
                            @foreach($budgets as $budget)
                                <option 
                                value="{{$budget->id}}" 
                                @if($budget->id == $budget->id) selected @endif
                                >
                                {{$budget->namePlan}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </form> --}}
                <form method="GET" action="{{action('BudgetController@index')}}">  
                    {{csrf_field()}}

                    <div style="float:right;font-size:12px;">
                        <select id="inputState" name="budget" style="margin-right:60px;height:25px;margin-top:-2px;">
                            @foreach($budgets as $budget)
                                <option 
                                value="{{$budget->id}}" 
                                >
                                {{$budget->namePlan}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                      <button type="submit" class="btn btn-primary" style="font-size:12px;margin-top:-4px;float:right;margin-right:-117px;">Search</button>
                </form>


                     {{-- @foreach($budgets as $budget)
                    {{$budget}}
                    @endforeach --}}
                  {{--   @if(empty($userPlan))
                        <p>Data does not exist</p>
                    @else
                        <p>HERE{{$userPlan}}</p>
                    @endif --}}
                    



                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="row">

                        <div class="col-sm-5">     
                            <canvas id="pieChart" style="max-width: 500px;margin-top:30px;"></canvas>
                            <div style="float:right;font-size:12px;font-family:Arial, Helvetica, sans-serif;margin-top:-35%;padding-left:10px;margin-right:-130px;">
                                <p><b>Livings <span style="font-size:10px;">(Eg:Food,Rent,Utilities,etc) :</span></b><span style="padding-left:5px;">{{$budget50Livings-$categoryExpenseLivings}}</span></p>
                                <p><b>Others <span style="font-size:10px;">(Eg:Entertainment,etc):</span></b><span style="padding-left:5px;">{{$budget30Others-$categoryExpenseOthers}}</span></p>
                                <p><b>Savings:</b><span style="padding-left:5px;">{{$budget20Savings-$categoryExpenseSavings}}</span></p>
                            </div>
                            <p style="padding-top:20px;padding-left:33%;font-size:12px;"><b>Budget Left Each Category</b></p>
                        </div>
                        <div class="col-sm-5">
                            <p style="font-size:12px;text-align:center;padding-top:5px;"><b>Budget Left</b></p>
                            <canvas id="chDonut1" style="margin-top:10px;margin-left:60px;"></canvas>
                            <div style="float:right;font-size:12px;font-family:Arial, Helvetica, sans-serif;margin-right:-75px;margin-top:-70px;">
                                @if(empty($try)&&empty($try2))

                                <p><b>Usage:</b><span style="padding-left:5px;"></span></p>
                                <p><b>Remainings:</b><span style="padding-left:5px;"></span></p>
                    
                                @else
                                <p><b>Usage (RM): </b><span style="padding-left:5px;">{{$try2->sum('amount')}}</span></p>
                                {{-- {{$try2->sum('amount')}} --}}
                                <p><b>Remainings (RM): </b><span style="padding-left:5px;">{{$try->budgetMonth-$try2->sum('amount')}}</span></p>
                                {{-- {{$try->budgetMonth-$try2->sum('amount')}} --}}

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Budget Modal Popup --}}
<div class="modal fade" id="planModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:rgba(0,0,0,.03);">
                <h5 class="modal-title" id="exampleModalCenterTitle" style="font-size:15px;font-family:Arial, Helvetica, sans-serif;">Recommendation Plan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>

          <form action="{{action('BudgetController@store')}}" method="POST">
            {{csrf_field()}}
            <div class="modal-body">
                <div class="form-group row" style="padding-top:10px;padding-bottom:10px;">
                    <label class="col-sm-4 col-form-label" style="font-size:14px;padding-top:15px;font-weight:600;">Budget Plan Name</label>
                    <div class="col-sm-6" style="width:70%;padding-top:10px;">
                        <input type="text" name="namePlan" class="form-control" style="width:80%;font-size:12px;height:30px;">
                    </div>

                </div>

                <div class="form-group row" style="padding-top:10px;padding-bottom:20px;">
                    <label class="col-sm-4 col-form-label" style="font-size:14px;padding-top:15px;font-weight:600;">Budget Monthly (RM)</label>
                    <div class="col-sm-6" style="width:70%;padding-top:10px;">
                        <input type="number" name="budgetMonth" class="form-control" style="width:80%;font-size:12px;height:30px;">
                    </div>
                </div>

                <div style="font-size:14px;font-weight:600;">Plan
                    <small style="padding-left:5px;">(Select one)</small>
                </div>

                <label style="padding-top:15px;font-size:12px;">
                    <input type="radio" name="post-format" id="post-format-plan"checked>
                    <span style="color:gray;padding-left:10px;">Recommendation Plan</span>
                </label>

                <label style="padding-left:13px;padding-top:15px;font-size:12px;">
                    <input type="radio" name="post-format" id="post-format-manual" value="Manual">
                    <span style="color:gray;padding-left:10px;">Manual</span>
                </label>

                <div class="form-group row" style="padding-top:10px;padding-bottom:20px;" id="plan-box">
                    <label class="col-sm-4 col-form-label" style="font-size:13px;padding-top:25px;font-weight:600;">                Recommendation Plan
                    </label>

                    <div class="col-sm-6" style="padding-top:22px;">
                        <select id="inputState" name="typePlan" class="form-control fa" style="font-size:15px;padding-top:2px;width:auto;">
                            <option value=""></option>
                            <option value="50-30-20">&#xf005; 50-30-20</option>
                            <option value="75-15-10">75-15-10</option>
                            <option value="60-10-30">60-10-30</option>
                        </select>

                    </div>
                </div>

                <div id="manual-box" style="padding-top:15px;padding-bottom:20px;">

                    <div class="form-group">
                      <label style="font-size:12px;">Living Expenses<small style="padding-left:10px;">(Eg:Utilities,Food,Transportation,etc)</small></label>
                      <input type="number" name="livingsBudget" class="form-control" style="font-size:12px;" placeholder="Eg:RM1500">
                  </div>

                  <div class="form-group">
                      <label style="font-size:12px;">Savings</label>
                      <input type="number" name="savingsBudget" class="form-control" style="font-size:12px;" placeholder="Eg:RM300">
                  </div>

                  <div class="form-group">
                      <label style="font-size:12px;">Others<small style="padding-left:10px;">(Eg:Entertainment,etc)</label>
                          <input type="number" name="othersBudget" class="form-control" style="font-size:12px;" placeholder="Eg:RM100">
                  </div>

                  </div>
              </div>

              <div class="modal-footer" style="margin-top:-30px;padding-top:0px;padding-bottom:0px;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size:12px;margin-top:13px;">Close</button>
                <button type="submit" class="btn btn-primary" style="font-size:12px;margin-top:10px;">Save changes</button>
            </div>
        }
    </form>
</div>
</div>
</div>

<div class="container" id="Report">
    <div>
        <div>
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size:15px;background-color:rgba(0,0,0,.03);font-family:Arial, Helvetica, sans-serif">Report

                    <button type="button" class="btn btn-outline-success"  onclick="window.location='{{ url("/report") }}'" style="margin-left:2%;" >View</button>
               
                </div>

                <div class="panel-body">
                    <form method="GET" action="{{action('ReportController@show')}}">  
                    {{csrf_field()}}

                        <input name="year" type="text" style="float:right;width:8%;font-size:12px;margin-right:60px;" placeholder="Eg:2020" >
                        <button type="submit" class="btn btn-primary" style="font-size:12px;margin-top:-2px;float:right;margin-right:-145px;">Search</button>
                    </form>

                    <div>
                        <div>     
                         <canvas id="myChart" style="width:658px;height:329px;"></canvas>
                         <div style="padding-bottom:40px;">
                            <p style="padding-top:20px;text-align:center;font-size:12px;width:100%;"><b>Year : {{$yearFilter}}</b></p>
                        </div>
                    </div>

                    <div>
                        <canvas id="lineChart"></canvas>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>


    
<div id="main"class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" >
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size:15px;background-color:rgba(0,0,0,.03);font-family:Arial, Helvetica, sans-serif">Summary Report</div>

                <div class="panel-body">
                    <div>
                        <p style="font-size:13px;"><b>Total Income (RM)</b><span style="font-size:13px;float:right;color:green;font-weight:600;">{{$incomes->sum('amount')}}</span></p>
                        <p style="font-size:13px;"><b>Total Expense(RM)</b><span style="font-size:13px;float:right;color:red;font-weight:600;">{{$expenses->sum('amount')}}</span></p>
                        <p style="font-size:13px;"><b>Gap (RM)</b> <span style="font-size:13px;float:right;font-weight:600;">{{$incomes->sum('amount') - $expenses->sum('amount')}}</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



    {{-- here --}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!-- Scripts -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    
    {{-- bootstrap css link and make toast problem --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    {{-- Radio Button Hide JQuery --}}
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    {{-- Radio Button JQuery --}}
    <script>
      $(function() {
        $('input[name=post-format]').on('click init-post-format', function() {
          $('#manual-box').toggle($('#post-format-manual').prop('checked'));
        }).trigger('init-post-format');
      });
      $(function() {
        $('input[name=post-format]').on('click init-post-format', function() {
          $('#plan-box').toggle($('#post-format-plan').prop('checked'));
        }).trigger('init-post-format');
      });

    </script>

    <script type="text/javascript">
        $(document).ready(function(){
           $('#datepicker').datepicker(); 
       });

    </script>
    <script type="text/javascript">
          $("#yearpicker").datepicker( {
            format: " yyyy", // Notice the Extra space at the beginning
            viewMode: "years", 
            minViewMode: "years"
          });
    </script>
    <script>


        //for side menu bar
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
          document.getElementById("main").style.marginLeft = "250px";
          document.getElementById("SmartTitle").style.marginLeft = "100px";
          document.getElementById("dashboard-home").style.marginLeft = "100px";
          document.getElementById("recommendationPlan").style.marginLeft = "300px";
          document.getElementById("Report").style.marginLeft = "300px";
          
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
          document.getElementById("main").style.marginLeft= "200px";
          document.getElementById("SmartTitle").style.marginLeft = "0";
          document.getElementById("dashboard-home").style.marginLeft = "0";
          document.getElementById("recommendationPlan").style.marginLeft = "200px";
          document.getElementById("Report").style.marginLeft = "200px";
        }
        
        function refresh(){
          var x = document.getElementById("snackbar");
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
          location.reload();
        }

        //tree view
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
          toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
      }
    </script>

    {{-- Expenses by category --}}
    <script>
        //pie
        var ctxP = document.getElementById("pieChart").getContext('2d');
        var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
        labels: ["Livings", "Others", "Savings"],
        datasets: [{
        data: [{{$budget50Livings-$categoryExpenseLivings}},{{$budget30Others-$categoryExpenseOthers}},{{$budget20Savings-$categoryExpenseSavings}}],
        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C"],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870"]
        }]
        },
        options: {
        responsive: true
        }
        });

    </script>
    <script>
        /* chart.js chart examples */
        // Donut Chart
        // chart colors
        var colors = ['#007bff','#28a745','#333333','#c3e6cb','#dc3545','#6c757d'];

        /* 3 donut charts */
        var donutOptions = {
          cutoutPercentage: 85, 
          legend: {position:'right', padding:10, labels: {pointStyle:'circle', usePointStyle:true}}
        };

        // donut 1

        var chDonutData1 = {
            labels: ['Usage', 'Remaining'],
            datasets: [
              {
                backgroundColor: colors.slice(0,2),
                borderWidth: 0,
                data: [{{$try2->sum('amount')}},{{$try->budgetMonth-$try2->sum('amount')}}]
              }
            ]

        }
  
        var chDonut1 = document.getElementById("chDonut1");
        if (chDonut1) {
          new Chart(chDonut1, {
              type: 'pie',
              data: chDonutData1,
              options: donutOptions
          });
        }

    </script>

    {{-- Bar chart Month --}}
    <script>
      var ctx = document.getElementById("myChart").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["January", "February", "March", "April", "May", "June","July","August","September","October","November","December"],
          datasets: [{
            label: 'Expenses Monthly',
            data: [{{$jan}},{{$feb}},{{$mar}},{{$apr}},{{$may}},{{$jun}},{{$jly}},{{$aug}},{{$sep}},{{$oct}},{{$nov}},{{$dec}}],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(0,255,255,0.2)',
            'rgba(128,0,0,0.2)',
            'rgba(128,0,128,0.2)',
            'rgba(255,0,255,0.2)',
            'rgba(0,0,128,0.2)',
            'rgba(192,192,192,0.2)',
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(0,255,255,1)',
            'rgba(128,0,0,1)',
            'rgba(128,0,128,1)',
            'rgba(255,0,255,1)',
            'rgba(0,0,128,1)',
            'rgba(192,192,192,1)',

            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    </script>

    {{-- Line Chart Year --}}
    <script>
      //line chart
      //line
      var ctxL = document.getElementById("lineChart").getContext('2d');
      var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
          labels: ["2019", "2020", "2021", "2022","2023"],
          datasets: [{
            label: "Total Expenses",
            data: [{{$totalYear2019}},{{$totalYear}},{{$totalYear2021}} ,{{$totalYear2022}},{{$totalYear2023}}],
            backgroundColor: [
            'rgba(105, 0, 132, .2)',
            ],
            borderColor: [
            'rgba(200, 99, 132, .7)',
            ],
            borderWidth: 2
          },

          ]
        },
        options: {
          responsive: true
        }
      });


    </script>
</body>
</html>





