<html>
<head>
    <title>Smart Expense</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/homesidebar.css') }}" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body style="background-color:#EEEEEE;">
    <div id="app">

        <nav class="navbars navbar-defaults navbar-static-tops" style="background-color:#00A0D2;position:fixed;width:100%;">
            <div class="container">
              {{--  <div class="navbar-headers">

                    <a class="navbar-brands" style="cursor:pointer;" href="{{ url('/') }}">
                        <span style="font-size:20px;font-family:Arial, Helvetica, sans-serif;color:white;margin-left:-100%;" class="SmartTitleHover" id="SmartTitle">Notification</span>
                    </a>

                </div> --}}
                <div style="float:right;padding-top:15px;margin-right:-15.8%;">
                  <span class="dropdown dropleft">
                  <i class="fa fa-bell" id="dropdownMenuButton"style="color:white;font-size:20px;padding-right:15px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"  style="background-color:#F5F6F7;width:250%;">
                    <div style="background-color:white"><h3 style="font-size:12px;font-family: inherit;padding:5px 0 5px 5px;">Notifications</h3></div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mix Rice</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM1</span><br><b>Total Expense: </b><span>RM9</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mamak</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM5</span><br><b>Total Expense: </b><span>RM5</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>
                    <div style="border-bottom:1px solid #dddfe2;padding-bottom:0px;"class="alert alert-warning" role="alert">
                      <p style="color:red;padding-bottom:10px;text-align: center;font-size:12px;">You had no take record today<i class="fa fa-frown" style="padding-left:10px;"></i></p>
                    </div>

                    <div style="border-bottom:1px solid #dddfe2;font-size:12px;background-color:#E5EAF2;padding-bottom:10px;" class="card">

                      <div class="card-header" style="display:flex;">
                        <div style="font-weight:700;text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;">Mamak</div>
                        <span style="color:blue;float:right;"><span style="padding-right:10px;">10am</span>2/12/2019</span>

                      </div>
                      <div style="background-color:#F5F6F7;">
                        <p class="dropdown-item"style="padding-top:15px;"><span style="color:red;">Expenses: </span><span> RM10</span><br><span style="color:green;">Income: </span><span>RM5</span><br><b>Total Expense: </b><span>RM5</span><br></p>
                      </div>

                    </div>

                    <div style="padding-top:10px;"></div>
                    <div style="padding-top:10px;background-color:white;">
                      <p style="text-align:center;margin-bottom:2px;">
                        <a class="dropdown-item" href="{{url('/notification')}}" style="color:blue;font-size:12px;margin-top:-10px;">See All</a>

                      </p>
                    </div>
                  </span>
                  </div>
                    <i class="fa fa-sync" onclick="refresh()" id="sync-hover" style="color:white;font-size:20px;padding-right:15px;"></i>
                    <i class="fa fa-user-circle" onclick="window.location='{{ url("/setting") }}'" id="user-hover" style="color:white;font-size:20px;padding-right:15px;"></i>
                </div>

                @include('top.sidebar')
            </div>
        </nav>
                <div style="padding-top:5%;padding-left:20%;padding-right:20%;">
                    {{-- <div class="modal-content"> --}}
                      <div style="padding-bottom:10px;">
                        <h5 style="font-size:14px;">Notification</h5>
                      </div>
                      
                       <div class="card" style="border-radius:1.25rem;">
                        <div class="card-header" style="display:flex;font-size:15px;">
                          <div>Mix Rice</div>
                          <span style="color:blue;padding-left:68%;font-size:15px;">
                            <span style="padding-right:10px;">10am</span>
                            2/12/2019
                          </span>

                        </div>
                        <div class="card-body"style="border-bottom:1px solid lightgray;display:flex;font-size:15px;">
                          <span>Description:</span>
                          <span style="text-overflow:ellipsis;overflow:hidden;width:300px;height:1.2em;white-space:nowrap;padding-left:10px;height:75px;">chicken , rice ,vegetables</span>
                        </div>
                        <div class="card-body" style="font-size:15px;">
                          <p style="color:red;">Expense: <span>RM10</span></p>
                          <p style="color:green;">Income:<span>RM2</span></p>
                          <p style="font-weight:700;">Total Expense:<span>RM8</span></p>
                        </div>
                      </div>
                  </div>
             </div>
         
                  

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


    <script>

        //for side menu bar
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
          document.getElementById("SmartTitle").style.marginLeft = "50px";
          document.getElementById("yourdetail-Setting").style.marginLeft = "250px";
          document.getElementById("yourName-Setting").style.marginLeft = "250px";
          document.getElementById("yourPassword-Setting").style.marginLeft = "250px";
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
          document.getElementById("SmartTitle").style.marginLeft = "-100px";
          document.getElementById("yourdetail-Setting").style.marginLeft = "0";
          document.getElementById("yourName-Setting").style.marginLeft = "0";
          document.getElementById("yourPassword-Setting").style.marginLeft = "0";
        }
        function refresh(){
          location.reload();
        }

        //tree view
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
          toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
      }
    </script>
</body>
</html>
