<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	// echo '<script>alert("Welcome to Geeks for Geeks")</script>';
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/category', 'CategoryController@store');
Route::post('/budget','BudgetController@store');
// Route::get('/budget/{id}','BudgetController@show');
Route::get('/budget','BudgetController@index');
Route::post('/homeExpense','HomeExpenseController@store');
// Route::get('/homeExpense','HomeExpenseController@index');
// Route::resource('/home', 'HomeController');

Route::resource('/expense','ExpenseController');
Route::resource('/income','IncomeController');


Route::get('/report','ReportController@index');
Route::get('/home/report','ReportController@show');
Route::get('/report/month','ReportController@reportShow');

//Change Password / Username
Route::get('/setting/changePassword','HomeController@showChangePasswordForm');
Route::post('/setting/changePassword','HomeController@changePassword')->name('changePassword');
Route::get('/setting/changeUsername',function(){
	return view('changename');
});
Route::post('/setting/changeUsername','HomeController@changeUsername')->name('changeUsername');

Route::get('/setting','UserController@profile');
Route::post('/setting','UserController@update_avatar');


//Upload Photo
// Route::post('/setting','UserController@update_avatar');



Route::get('/document',function(){
	return view('documentContent');
});
Route::get('/notification',function(){
	return view('notification');
});















//testing purpose
Route::get('/test', function () {
    return view('testing');
});

Route::post('/submitdata','TestController@testfunction');

// Route::get('view-records','TestController@index');

//cannot
// Route::get('/home/{id}',
// 'TestController@show')->name('layouts.toastLogin');
///above testing